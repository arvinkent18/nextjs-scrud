import React from 'react';
import Layout from '../layouts/Main';
import { getPosts } from '../api/posts';
import { Link } from '../routes';

import Post from '../components/Post';

const Index = ({ posts }) => (
	<Layout>
		<ul>{posts.map((p) => <Post key={p.title} post={p} />)}</ul>
	</Layout>
);

Index.getInitialProps = async ({ req }) => {
	const res = await getPosts();
	const json = await res.json();

	return { posts: json };
};

export default Index;
